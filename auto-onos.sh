#!/bin/bash

# Configuração do logger
logger() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') - $1"
}

# Verifica se o Docker está instalado e exibe sua versão
function check_docker {
    if command -v docker &>/dev/null; then
        echo "Docker já está instalado."
        docker --version
    else
        echo "Docker não está instalado."
        install_docker
    fi
}

# Instala o Docker
install_docker() {
    logger "Tentando instalar o Docker"
    sudo apt-get install -y docker.io
    logger "Docker instalado com sucesso."
}

# Verifica se o Mininet está instalado e exibe sua versão
function check_mininet {
    if command -v mn &>/dev/null; then
        echo "Mininet já está instalado."
        mn --version
    else
        echo "Mininet não está instalado."
        install_mininet
    fi
}

# Instala o Mininet
install_mininet() {
    logger "Tentando instalar o Mininet"
    sudo apt-get install -y mininet
    logger "Mininet instalado com sucesso."
}

start_onos_server() {
    container_name="onos"
    logger "Iniciando o servidor ONOS no Docker"
    if docker ps -a --format '{{.Names}}' | grep -q "^$container_name$"; then
        logger "O contêiner com o nome '$container_name' já está em uso. Tentando pará-lo e removê-lo..."
        stop_and_remove_container $container_name
        if [ $? -eq 0 ]; then
            logger "Contêiner '$container_name' removido com sucesso. Tentando iniciar o servidor ONOS novamente..."
        else
            logger "Erro ao remover o contêiner '$container_name'. O servidor ONOS não pôde ser iniciado."
            return 1
        fi
    fi
    docker run -d --name $container_name -p 8181:8181 -p 6653:6653 -p 8101:8101 onosproject/onos
    logger "Servidor ONOS iniciado com sucesso."
}

# Para e remove o contêiner Docker
stop_and_remove_container() {
    docker stop $1 && docker rm $1
}

# Abre o servidor web na porta 8181
open_web_server() {
    xdg-open http://localhost:8181/onos/ui/login.html
    logger "Servidor web ONOS aberto com sucesso."
}

# Executa o comando para criar a topologia Mininet
start_mininet_hosts() {
    logger "Criando topologia usando o controlador ONOS."
    sudo mn -c
    sudo mn --topo=tree,depth=2 --controller=remote,ip=127.0.0.1,port=6653 --switch=ovsk,protocols=OpenFlow13
    logger "Topologia criada com sucesso."
}

# Função para aguardar e executar uma mensagem
delay_and_execute() {
    message="$1"
    seconds="$2"
    for (( i=$seconds; i>0; i-- )); do
        logger "$message: $i segundos restantes"
        sleep 1
    done
}

section_separator(){
    echo "--------------------------------------------"
}

scanner_host(){
    echo "INICIANDO AUTOMAÇÃO DE ATAQUE - GT-FAIR-5G"
    echo "--------------------------------------------"
    
    # Recuperando informações sobre a máquina
    echo "HOST INFORMATION:"
    echo "--------------------------------------------"
    echo "RAM: $(grep MemTotal /proc/meminfo | awk '{print $2/1024 " MB"}')"
    echo "AVAILABLE STORAGE: $(df -h --output=size / | tail -n 1)"
    echo "CPU: $(lscpu | grep 'Model name' | awk -F ': ' '{print $2}')"
    echo "CPU CORES: $(lscpu | grep 'CPU(s):' | awk '{print $2}')"
    echo "CPU ARCHITECTURE: $(lscpu | grep 'Architecture' | awk '{print $2}')"
    echo "CPU SPEED: $(lscpu | grep 'CPU MHz' | awk '{print $3 " MHz"}')"
    echo "--------------------------------------------"
}

# Função principal
main() {
    scanner_host
    check_docker
    section_separator
    check_mininet
    section_separator
    start_onos_server
    section_separator
    delay_and_execute "Abrindo ONOS web Service" 10
    section_separator
    open_web_server
    section_separator
    delay_and_execute "Criando Topologia" 5
    section_separator
    start_mininet_hosts
}

# Executa a função principal
main
