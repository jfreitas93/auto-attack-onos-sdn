**Automatização de Testes de Segurança para Redes SDN com ONOS**

Este repositório contém scripts (ainda em desenvolvimento) em Bash para automatizar testes de segurança em redes definidas por software (SDN), com foco no controlador ONOS. A automação visa facilitar a identificação e mitigação de vulnerabilidades em ambientes SDN, proporcionando uma estrutura flexível e automatizada para conduzir testes de segurança com objetividade e didática.

**Recursos:**
- **Exibição de informações do host:** Recuperação de informações sobre a máquina local, incluindo RAM, armazenamento disponível e especificações da CPU.
- **Verificação e instalação do Docker e Mininet:** Verificação da presença e, se necessário, instalação do Docker e do Mininet, duas dependências importantes para a execução do controlador ONOS e a simulação de redes SDN.
- **Inicialização do servidor ONOS em um contêiner Docker:** Configuração e inicialização do servidor ONOS em um contêiner Docker para fornecer um ambiente controlado para testes de segurança.
- **Acesso ao servidor web ONOS:** Abertura do servidor web ONOS na porta dedicada para permitir o acesso à interface gráfica e interação com o controlador SDN.
- **Autenticação no servidor ONOS em ambiente de homologação:** Possibilidade de autenticar-se na interface web do ONOS, conforme necessário para acessar suas funcionalidades.
- **Autorização de apps e protocolos:** Autorização de aplicativos e protocolos, como OpenFlow e P4, conforme exigido pelas configurações do ambiente de homologação.
- **Criação de uma topologia Mininet com ONOS:** Configuração de uma topologia Mininet com o controlador ONOS para realizar testes de segurança usando os protocolos OpenFlow e P4.
- **Testes e ataques de inundação iniciais:** Realização de testes e ataques de inundação iniciais para visualizar o comportamento do controlador e identificar possíveis vulnerabilidades na rede.

**Como Usar:**
1. Clone este repositório em sua máquina local.
2. Dê permissão de execução ao script `auto-onos.sh` com o comando `chmod +x auto-onos.sh`.
3. Execute o script `auto-onos.sh` para iniciar a automação dos testes de segurança.

**Requisitos:**

- Conexão com a Internet para baixar e instalar dependências.
- Permissões de administrador para instalar e configurar o Docker, se necessário.
- Sistema operacional compatível com os comandos utilizados no script (Linux recomendado).

**Contribuição:**
Contribuições são bem-vindas! Sinta-se à vontade para enviar pull requests com melhorias, correções de bugs ou novos recursos.

**Autores:**
[Johan Kevin](https://gitlab.com/jfreitas93)
[Ryan]
[Francisco]

**Licença:**
Este projeto é licenciado sob a [Licença MIT](https://opensource.org/licenses/MIT).